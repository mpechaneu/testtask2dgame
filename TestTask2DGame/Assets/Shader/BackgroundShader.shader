﻿Shader "Custom/BackgroundShader"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap("Pixel Snap", Float) = 0
		[MaterialToggle] _StopMovement("Stop Movement", Float) = 0
		_ScrollSpeed("Scroll Speed", Range(0, 4)) = 1
		[PerRendererData]_ScrollValue("Scroll Value", Float) = 0
	}

		SubShader
	{
		Tags
	{
		"Queue" = "Transparent"
		"IgnoreProjector" = "True"
		"RenderType" = "Transparent"
		"PreviewType" = "Plane"
		"CanUseSpriteAtlas" = "True"
	}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha

		Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma multi_compile DUMMY PIXELSNAP_ON
#pragma enable_d3d11_debug_symbols
#include "UnityCG.cginc"

		struct appdata_t
	{
		float4 vertex   : POSITION;
		float4 color    : COLOR;
		float2 texcoord : TEXCOORD0;
	};

	struct v2f
	{
		float4 vertex   : SV_POSITION;
		fixed4 color : COLOR;
		half2 texcoord  : TEXCOORD0;
	};

	fixed4 _Color;
	float _ScrollSpeed;
	sampler2D _MainTex;
	float _StopMovement;
	float _ScrollValue;

	v2f vert(appdata_t IN)
	{
		v2f OUT;
		OUT.vertex = UnityObjectToClipPos(IN.vertex);
		fixed2 scrolledUV = IN.texcoord;
		if (_StopMovement == 0)
		{
			_ScrollValue = _ScrollSpeed * _Time.x;
			scrolledUV.x += _ScrollValue;
		}
		OUT.texcoord = scrolledUV;
		OUT.color = IN.color * _Color;
#ifdef PIXELSNAP_ON
		OUT.vertex = UnityPixelSnap(OUT.vertex);
#endif

		return OUT;
	}

	fixed4 frag(v2f IN) : COLOR
	{
		half4 prev = IN.color * tex2D(_MainTex, IN.texcoord);
		return prev;
	}
		ENDCG
	}
	}
}