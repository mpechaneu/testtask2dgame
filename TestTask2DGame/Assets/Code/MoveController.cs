﻿using UnityEngine;

namespace Code
{
    public class MoveController: MonoBehaviour
    {
        private const float MoveSpeed = 10;
        private const float Acceleration = 20;
        private const float BorderModifier = 0.3f;

        private float _moveBorder;
        public GameOverService GameOverService;
        public TimeService TimeService;

        void Awake()
        {
            Camera cam = Camera.main;
            _moveBorder = cam.orthographicSize;
            _moveBorder -= BorderModifier;
        }

        void Update()
        {
            var deltaY = Input.GetAxis("Vertical") * MoveSpeed * Time.deltaTime;
            var yPosition = Mathf.Clamp(transform.position.y + deltaY, -_moveBorder, _moveBorder);
            transform.position = new Vector2(transform.position.x, yPosition);

            var deltaX = Input.GetAxis("Horizontal") * Acceleration * Time.deltaTime;
            TimeService.ChangeTimeScale(deltaX);
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (IsMonster(other))
            {
                GameOverService.OnGameOver();
            }
        }

        private bool IsMonster(Collider2D col)
        {
            return col.gameObject.GetComponent<MonsterController>() != null;
        }
    }
}
