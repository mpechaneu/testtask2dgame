﻿using UnityEngine;
using UnityEngine.UI;

namespace Code
{
    public class GameOverService : MonoBehaviour
    {
        public Button GameOverButton;
        public GameObject Monsters;
        public GameObject Bullets;

        private TimeService _timeService;

        void Awake()
        {
            GameOverButton.gameObject.SetActive(false);
            _timeService = GetComponent<TimeService>();
        }

        public void OnGameOver()
        {
            _timeService.PauseGame();
            GameOverButton.gameObject.SetActive(true);
        }

        public void RestartGame()
        {
            GameOverButton.gameObject.SetActive(false);
            foreach (Transform monsterTransform in Monsters.transform)
            {
                Destroy(monsterTransform.gameObject);
            }
            foreach (Transform bulletTransform in Bullets.transform)
            {
                Destroy(bulletTransform.gameObject);
            }
            _timeService.Resume();
        }
    }
}
