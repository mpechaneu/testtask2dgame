﻿using UnityEngine;

namespace Code
{
    public class BulletController : MonoBehaviour
    {
        private const float BulletSpeed = 20;
        private bool _destroyed = false;

        public void OnBecameInvisible()
        {
            DestroyInternal();
        }

        void Start () {
		    GetComponent<Rigidbody2D>().AddForce(new Vector2(BulletSpeed,0), ForceMode2D.Impulse);
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (IsMonster(other))
            {
                DestroyInternal();
            }
        }

        private bool IsMonster(Collider2D col)
        {
            return col.gameObject.GetComponent<MonsterController>() != null;
        }

        private void DestroyInternal()
        {
            if (_destroyed)
                return;
            _destroyed = true;
            Destroy(gameObject);
        }
    }
}
