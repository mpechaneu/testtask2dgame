﻿using UnityEngine;

namespace Code
{
    public class ShootController: MonoBehaviour
    {
        public GameObject TopBulletPrefab;
        public GameObject BotBulletPrefab;
        public GameObject BulletPlace;
        public TimeService TimeService;

        public void Update()
        {
            if (TimeService.IsPaused())
                return;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Fire();
            }
        }

        private void Fire()
        {
            var obj = Instantiate(TopBulletPrefab, transform);
            obj.transform.SetParent(BulletPlace.transform);
            obj = Instantiate(BotBulletPrefab, transform);
            obj.transform.SetParent(BulletPlace.transform);
        }
    }
}
