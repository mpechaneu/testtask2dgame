﻿using UnityEngine;

namespace Code
{
    public class TimeService: MonoBehaviour
    {
        private const float TimeMinScale = 0.5f;
        private const float TimeMaxScale = 1.5f;

        private bool _paused = false;

        public void PauseGame()
        {
            Time.timeScale = 0;
            _paused = true;
        }

        public void Resume()
        {
            Time.timeScale = 1;
            _paused = false;
        }

        public bool IsPaused()
        {
            return _paused;
        }

        public void ChangeTimeScale(float timeModifier)
        {
            if (_paused)
                return;
            Time.timeScale = Mathf.Clamp(1 + timeModifier, TimeMinScale, TimeMaxScale);
        }
    }
}
