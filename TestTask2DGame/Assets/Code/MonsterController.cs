﻿using UnityEngine;

namespace Code
{
    public class MonsterController : MonoBehaviour {

        public float MonsterSpeed = 5;
        public float MonsterHp = 1;

        private bool _destroyed = false;

        // Update is called once per frame
        void Start()
        {
            GetComponent<Rigidbody2D>().AddForce(new Vector2(-MonsterSpeed, 0), ForceMode2D.Impulse);
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (IsBullet(other))
            {
                DealDamage();
            }
        }

        private void DealDamage()
        {
            MonsterHp--;
            if (MonsterHp <= 0)
            {
                DestroyInternal();
            }
        }

        private bool IsBullet(Collider2D other)
        {
            return other.GetComponent<BulletController>() != null;
        }

        void OnBecameInvisible()
        {
            DestroyInternal();
        }

        private void DestroyInternal()
        {
            if (_destroyed)
                return;
            _destroyed = true;
            Destroy(gameObject);
        }
    }
}
