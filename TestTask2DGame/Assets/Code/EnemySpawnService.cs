﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Code
{
    public class EnemySpawnService: MonoBehaviour
    {
        private const float SpawnSpeed = 10;
        private const float MinSwapnTime = 10;
        private const float MaxSwapnTime = 20;
        private const int MinMonster = 5;
        private const int MaxMonster = 8;
        private const int PlaceSeparation = 10;

        public List<GameObject> MonsterPrefabs; 

        private float _time = MinSwapnTime;
        private const float BorderModifier = 0.3f;
        private const float SpawnPositionModifier = 0.3f;

        private float _moveBorder;
        private float _spawnPosition;

        void Awake()
        {
            Camera cam = Camera.main;
            float height = 2f * cam.orthographicSize;
            float width = height * cam.aspect;
            _moveBorder = cam.orthographicSize;
            _moveBorder -= BorderModifier;
            _spawnPosition = width + SpawnPositionModifier;
        }

        void Update()
        {
            _time -= Time.deltaTime * SpawnSpeed;
            if (_time <= 0)
            {
                SpawnMonsters();
                UpdateTimer();
            }
        }

        private void SpawnMonsters()
        {
            int monsterCount = Random.Range(MinMonster, MaxMonster + 1);
            var spawnPositions = GetSpawnPositions(monsterCount);
            foreach (var spawnPosition in spawnPositions)
            {
                var monsterType = Random.Range(0, MonsterPrefabs.Count);
                var monster = Instantiate(MonsterPrefabs[monsterType], transform, false);
                monster.transform.position = new Vector2(_spawnPosition, -_moveBorder + spawnPosition * 2 * _moveBorder / (PlaceSeparation - 1));
            }
        }

        private List<int> GetSpawnPositions(int monsterCount)
        {
            List<int> positions = new List<int>();
            List<int> spawnPositions = new List<int>();
            int i;
            if (PlaceSeparation < monsterCount)
                throw new Exception("too many monsters to generate");
            for (i = 0; i < PlaceSeparation; i++)
            {
                positions.Add(i);
            }
            for (i = 0; i < monsterCount; i++)
            {
                var indexToSwap = positions.Count - 1 - i;
                var randomIndex = Random.Range(0, positions.Count - i);
                var savePosition = positions[randomIndex];
                positions[randomIndex] = positions[indexToSwap];
                positions[indexToSwap] = savePosition;
                spawnPositions.Add(positions[indexToSwap]);
            }
            return spawnPositions;
        } 

        private void UpdateTimer()
        {
            _time = MinSwapnTime + Random.value*(MaxSwapnTime - MinSwapnTime);
        }
    }
}
